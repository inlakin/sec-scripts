#! /usr/bin/python

'''

 SMTP checher: 
 
Usage:
	Read a list of IP from a file
	smtp.py -f ips.txt
	
	Execute the script on a single IP
	smtp.py <IP>

 Following a scan where the port 25 has been identified as open, we can launch
 this script which will perform a series of checks:

 - Check the VRFY command
	-if yes, attemps to bruteforce some usernames with a predefined list.
 - Check if the SMTP is a relay
 -

'''

import os
import socket
import sys

PORT = 25
USERNAME = 'admin'
FILE = 'usernames.txt'

def usage():
	if len(sys.argv) != 2:
		print "[*] Usage: " + sys.argv[0] + "<IP>"
		sys.exit(0)

def banner():

	print "*********************************************************************"
	print "*                                                                   *"
	print "*   _____ __  _____________     ________              __            *"
	print "*  / ___//  |/  /_  __/ __ \   / ____/ /_  ___  _____/ /_____  _____*"
	print "*  \__ \/ /|_/ / / / / /_/ /  / /   / __ \/ _ \/ ___/ //_/ _ \/ ___/*"
	print "* ___/ / /  / / / / / ____/  / /___/ / / /  __/ /__/ ,< /  __/ /    *"
	print "*/____/_/  /_/ /_/ /_/       \____/_/ /_/\___/\___/_/|_|\___/_/     *"
	print "*                                                                   *"
	print "*                                                                   *"
	print "*                       Robin Desir - 2018                          *"
	print "*                                                                   *"
	print "*********************************************************************"

def openConnection(ip):

	try:
		# Checking if sys.argv[1] is an IP address
		socket.inet_aton(ip)
		print "[*] Target: " + ip + ""
		print ""
		print "[*] Attempting connection to " + ip + ":" + str(PORT) + "..."
		s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		
		try:
			s.settimeout(20)
			connect=s.connect((ip, PORT))
			print "[*] Connected to " + ip
			print "[*] Waiting for banner ..."
			banner = s.recv(1024)
			print "[<] " + banner

			# HELO cmd
			print "[*] Sending HELO command ..."
			s.send('HELO ' + USERNAME + "\r\n")
			result = s.recv(1024)
			print "[<] " + result
			# HELP cmd
			print "[*] Sending HELP command ..."
			s.send('HELP' + "\r\n")
			result = s.recv(1024)

			if ("not supported" in result) or ("not recognized" in result):
				print "[-] HELP command is not supported by target .. :("
				print "[*] Trying VRFY anyway ... "
				s.send('VRFY ' + USERNAME + "\r\n")
				result = s.recv(1024)
				if ("disallowed" in result) or ("not supported" in result):
					print "[-] VRFY command is either not supported or disallowed by server."
				else:
					print "[*] VRFY command is enabled ! Trying username bruteforce ..."

					f = open(FILE,'r')
					print "[*] Bruteforcing based on file: '" + FILE + "'"
					for user in f:
						s.send('VRFY '+user.rstrip('\n') + "\r\n")
						result = s.recv(1024)
						if "252" in result:
							print "[+] Found valid user: " + user.rstrip('\n')

			else:
				print "[*] Parsing HELP command output ..."
				print result
				if "VRFY" in result:
					print "[+] VRFY command is supported."
					print "[>] Sending VRFY command with username '" + USERNAME + "'."
					s.send('VRFY ' + USERNAME + "\r\n")
					result = s.recv(1024)
					if "disallowed" in result:
						print "[-] VRFY is disallowed on target .. :("
					else:
						print result
				else:
					print "[-] VRFY command is either not supported or disallowed by server."

		except socket.error:
			print "[-] Failed to connect to SMTP server: " + ip + ":" + str(PORT)
			print "[*] Exiting." 
			sys.exit(0)
	
	except socket.error:
		print "[-] '" + ip + "' is not an IP address!"
		print "[*] Exiting." 
		sys.exit(0)

	

if __name__ == '__main__':
	usage()
	banner()
	openConnection(sys.argv[1])