#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 root <root@inlakin-oscp>
#
# Distributed under terms of the MIT license.

"""


"""

import os
import sys
import subprocess
import socket
import re

def usage():
    print "Usage: " + sys.argv[0] + " <IP> "
    print "Example:"
    print "\t" + sys.argv[0] + " 10.1.1.0/24"
    print "\t" + sys.argv[0] + " 10.1.1.10"

def parse_target(target):
    print "[*] Starting network scan on target : " + target

def start(ip_list):
    for ip in ip_list:
        print "[*] Initiating based scan for %s" % ip

def main():

    if len(sys.argv) != 2:
        usage()
        exit(0)

    target = sys.argv[1]

    if "/" in target:
        r = raw_input("[*] About to launch a recon scan on a network range, do you confirm ? [O/n] : ")
        if r == "O" or r == "o" or r == "Y" or r == "y":
            parse_target(target)
        else:
            print "[*] Aborting scan. Exiting."
            exit(0)

main()
