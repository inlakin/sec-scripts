#! /bin/sh
#
# recon.sh
# Copyright (C) 2018 root <root@inlakin-oscp>
#
# Distributed under terms of the MIT license.
#
# First scan launched:  nmap -sV -sS -Pn -v --open $ip -oA $ip.nmap-short-tcp
#
#


parse_output(){
  egrep -v "^#|Status: Up" $1 | cut -d' ' -f2,4- | \
  sed -n -e 's/Ignored.*//p'  | \
  awk '{print "Host: " $1 " Ports: " NF-1; $1=""; for(i=2; i<=NF; i++) { a=a" "$i; }; split(a,s,","); for(e in s) { split(s[e],v,"/"); printf "%-8s %s/%-7s %s\n" , v[2], v[3], v[1], v[5]}; a="" }' | \
  grep open |\
  cut -d"/" -f2 |\
  cut -d " " -f2 > $ip-opened-ports.lst
}


if [ -z "$1" ]; then
  echo "[*] Usage : $0 <file>"
  echo "Where <file> contains a list of IP"
  exit 0
fi

echo "[*] Reading IP from $1"
echo "[*] Found $(cat $1 | wc -l) IP"
echo "[*] Creating folder structure .."

# Launch scans
for ip in $(cat $1); do
  if [ ! -f $ip ]; then
    mkdir $ip
  fi
  echo "[*] Scanning $ip"
  nmap -sV -sS -Pn --open $ip -oA $ip/$ip &>/dev/null
done

# Wait for all previous subprocess to end
wait

echo "[*] Parsing scan results ..."
# Parse scan output
for ip in $(cat $1);do
  # Creating opened ports file for each scanned IP
  parse_output "$ip/$ip.gnmap"
  # Loop within each ports and launch appropriate scans
  echo "[*] Searching through opened ports ..."
  for port in $(cat $ip-opened-ports.lst); do

    echo $port
  done
done



